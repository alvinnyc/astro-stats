import numpy as np
from matplotlib import pyplot as plt
from astropy.io import fits
from astroML.plotting import scatter_contour
#from astroML.plotting import setup_text_plot


hdulist=fits.open('allskyfuv_all.fits')
hdulist.info()
number=12582912
number1=125829
def get_ir_uv(num_ir=12582912,num_uv=12582912):
    data=hdulist[1].data
   # print data[0]
    ir_raw=data['FUV'][:num_ir]
    uv_raw=data['IR'][:num_uv]
    lat_raw=data['LATITUDE'][:num_ir] 
    p=np.where(ir_raw!=0)
    ir_nan=ir_raw[p]
    uv_nan=uv_raw[p]
    lat_nan=lat_raw[p]
    pp=np.where(~np.isnan(ir_nan))
    ir_nocut=ir_nan[pp]
    uv_nocut=uv_nan[pp]
    lat_nocut=lat_nan[pp]
    ppp=np.where(15<=np.abs(lat_nocut))
    ir=ir_nocut[ppp]
    uv=uv_nocut[ppp]
    return ir,uv


def plot_ir_uv(ir,uv):
    plot_kwargs=dict(color='k',linestyle='none',marker=',')
    fig=plt.figure(figsize=(15,10.75))

    
    ax1=fig.add_subplot(511)
    ax1.plot(uv,ir,**plot_kwargs)
    ax2=fig.add_subplot(513)
    ax2.hist(uv,bins=20,normed=1,facecolor='g',alpha=0.75)
    ax3=fig.add_subplot(515)
    ax3.hist(ir,bins=20,normed=1,facecolor='g',alpha=0.75) 

#    ax1.set_xlim(-100,100)
    #ax1.set_ylim(0,40) 
    ax1.set_ylabel('FUV')
    ax1.set_xlabel('IR 150 um')
    ax1.set_title('FUV versus IR')
    ax2.set_title('IR histogram')
    ax2.set_ylabel('probability')
    ax2.set_xlabel('IR')
    ax3.set_title('FUV histogram')
    ax3.set_xlabel('FUV')
    ax3.set_ylabel('probability')
   # for ax in (ax1,ax2,ax3):
    #    ax.xaxis.set_major_locator(plt.MultipleLocator(1))


         #   ax.yaxis.set_major_locator(plt.MultipleLocator(1)) 

def plot_ir_uv_scatter(ir,uv):
    plot_kwargs=dict(color='k',linestyle='none',marker=',')
    #fig=plt.figure(figsize=(15,10.75))

    ax1=plt.axes()
    area=np.pi*(12)**2
    ax1.scatter(uv,ir, s=area,alpha=0.1)
   # plt.show()

 #   ax1.set_xlim(-100,100)
#    ax1.set_ylim(0,40)
    ax1.set_ylabel('uv')
    ax1.set_xlabel('longtitute')
    ax1.set_title('FUV versus longtitute scatter plot')

def plot_ir_uv_scatter_countor(ir,uv):
   
    #fig=plt.figure(figsize=(15,10.75))
    
    ax1=plt.axes()
 
    scatter_contour(uv, ir, threshold=400, log_counts=True, histogram2d_args=dict(bins=40), plot_args=dict(marker=',', linestyle='none', color='black'), contour_args=dict(cmap=plt.cm.bone))


  #  ax1.set_xlim(-100,100)
#    ax1.set_ylim(0,40)
    ax1.set_ylabel('FUV')
    ax1.set_xlabel('')
    ax1.set_title('FUV versus IR scatter countor plot')


def plot_ir_uv_scatter_hess(ir,uv):

            #fig=plt.figure(figsize=(15,10.75))

 
    H, xbins, ybins = np.histogram2d(uv, ir, bins=(np.linspace(0.0, 5000, 100), np.linspace(0.0, 40, 100)))
 # Create a black and white color map where bad data (NaNs) are white
    cmap = plt.cm.binary
    cmap.set_bad('w', 1.)
# Use the image display function imshow() to plot the result
    fig, ax1 = plt.subplots(figsize=(5, 3.75))
    H[H == 0] = 1  # prevent warnings in log10
    ax1.imshow(np.log10(H).T, origin='lower', extent=[xbins[0], xbins[-1], ybins[0], ybins[-1]], cmap=cmap, interpolation='nearest', aspect='auto')
     

            
#    ax1.set_xlim(-100,100)
#    ax1.set_ylim(0,40)
    ax1.set_ylabel('FUV')
    ax1.set_xlabel('IR')
    ax1.set_title('FUV versus IR scatter hess diagram')
def plot_ir_uv_scatter_hess_color(ir,uv):

    #fig=plt.figure(figsize=(15,10.75))


    H, xbins, ybins = np.histogram2d(uv, ir, bins=(np.linspace(0.0, 5000, 100), np.linspace(0.0, 40, 100)))
    # Create a black and white color map where bad data (NaNs) are white
    cmap_multicolor = plt.cm.jet
    cmap_multicolor.set_bad('w', 1.)
    # Use the image display function imshow() to plot the result
    fig, ax1 = plt.subplots(figsize=(5, 3.75))
    H[H == 0] = 1  # prevent warnings in log10
    ax1.imshow(np.log10(H).T, origin='lower', extent=[xbins[0], xbins[-1], ybins[0], ybins[-1]], cmap=cmap_multicolor, interpolation='nearest', aspect='auto')
        
        
        
  #  ax1.set_xlim(-100,100)
#    ax1.set_ylim(0,40)
    ax1.set_ylabel('FUV')
    ax1.set_xlabel('IR')
    ax1.set_title('FUV versus IR scatter hess diagram')
    #cb=plt.colorbar()
    #cb.set_label(r'$\mathrm{number\ in\ pixel}$')

    #plt.clim(-2.5, 0.5)
    

    
def plot_ir_uv_scatter_hess_color_3d(ir,uv):

    #fig=plt.figure(figsize=(15,10.75))
    # Plot the results using the binned_statistic function
    from astroML.stats import binned_statistic_2d
    from astroML.stats import binned_statistic
        

    N, xedges, yedges = binned_statistic_2d(uv, ir, ir,'count', bins=[100,100])
   
    ir_median, xedges,= binned_statistic(uv, ir,'median', bins=100)
    ir_std, xedges,= binned_statistic(uv, ir,np.std(ir), bins=100)
        
#    N[N == 0] = 1
    np.savetxt('median_fuvir',ir_median)
       
    # Create a black and white color map where bad data (NaNs) are white

    cmap = plt.cm.copper
    cmap.set_bad('w', 1.)

    cmap_multicolor = plt.cm.jet
    cmap_multicolor.set_bad('w', 1.)
# Use the image display function imshow() to plot the result
    fig, ax1 = plt.subplots(figsize=(5, 3.75))
    plt.imshow(np.log10(N.T), origin='lower', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]], aspect='auto', interpolation='nearest', cmap=cmap_multicolor)
#    plt.imshow(ir_median.T, origin='lower', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]], aspect='auto', interpolation='nearest', cmap=cmap_multicolor)
    plt.xlim(xedges[-1], xedges[0])
    plt.ylim(yedges[-1], yedges[0])            
   # plt.contour(ir_median.T, colors='k', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]])
    area=np.pi*(7)**2
    xedgesnew=np.delete(xedges,100,0)
    np.savetxt('xedge_fuvir',xedgesnew)
    print xedgesnew.shape
    print ir_median.shape
    ir_median.shape
    ax1.scatter(xedgesnew,ir_median, s=area,alpha=0.5)

         
    ax1.set_ylabel('FUV')
    ax1.set_xlabel('IR')
    ax1.set_title('FUV versus IR scatter hess diagram')
    cb = plt.colorbar(ticks=[0, 1, 2, 3], pad=0.2, format=r'$10^{%i}$', orientation='horizontal')
    cb.set_label(r'$\mathrm{means\ in\ pixel}$')
            

    
ir,uv = get_ir_uv()
#plot_ir_uv(ir,uv)
#plot_ir_uv_color(ir,uv)
#plot_ir_uv_scatter(ir,uv)
#plot_ir_uv_scatter_countor(ir,uv)
#plot_ir_uv_scatter_hess(ir,uv)

#plot_ir_uv_scatter_hess_color(ir,uv)
plot_ir_uv_scatter_hess_color_3d(ir,uv)
plt.show()
